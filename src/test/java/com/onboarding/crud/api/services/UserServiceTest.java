package com.onboarding.crud.api.services;

import com.onboarding.crud.api.dto.UserDto;
import com.onboarding.crud.api.dto.UserInsertDto;
import com.onboarding.crud.api.entities.User;
import com.onboarding.crud.api.exceptions.EmailAlreadyUsedException;
import com.onboarding.crud.api.exceptions.ResourceNotFoundException;
import com.onboarding.crud.api.exceptions.UsernameAlreadyUsedException;
import com.onboarding.crud.api.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class UserServiceTest {

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private UserService userService;

	@BeforeEach
	public void setUp () {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void givenValidInput_whenSearchingUserByUsername_thenReturnUserData () {
		User user1 = new User(
				"id1",
				"test@test.com",
				"John Doe",
				"password",
				"johndoe");

		UserDto validator = new UserDto(user1);

		when(userRepository.findByUsername("johndoe")).thenReturn(Optional.of(user1));

		UserDto dto = userService.findByUsernameOrEmail("johndoe");

		assertEquals(dto, validator);
	}

	@Test
	public void givenInvalidInput_whenSearchingUserByUsername_thenThrowNewException () {


		when(userRepository.findByUsername("janedoe")).thenReturn(Optional.empty());


		assertThrows(ResourceNotFoundException.class, () -> userService.findByUsernameOrEmail("janedoe"));
	}

	@Test
	public void givenValidInput_whenSearchingUserByEmail_thenReturnUserData () {
		User user1 = new User(
				"id1",
				"test@test.com",
				"John Doe",
				"password",
				"johndoe");

		UserDto validator = new UserDto(user1);

		when(userRepository.findByEmail("test@test.com")).thenReturn(Optional.of(user1));

		UserDto dto = userService.findByUsernameOrEmail("test@test.com");

		assertEquals(dto, validator);
	}

	@Test
	public void givenInvalidInput_whenSearchingUserByEmail_thenThrowNewException () {


		when(userRepository.findByEmail("test2@test.com")).thenReturn(Optional.empty());

		assertThrows(ResourceNotFoundException.class, () -> userService.findByUsernameOrEmail("test2@test.com"));
	}

	@Test
	public void givenValidInput_whenInsertingNewUser_thenReturnDto () {
		UserInsertDto insertDto = new UserInsertDto(
				"test@test.com",
				"John Doe",
				"password",
				"johndoe");

		User user1 = new User(
				"id1",
				"test@test.com",
				"John Doe",
				"password",
				"johndoe");

		when(userRepository.save(user1)).thenReturn(user1);

		UserDto result = userService.insertNewUser(insertDto);

		assertNotNull(result);
		assertEquals(insertDto.email(), result.getEmail());
		assertEquals(insertDto.name(), result.getName());
		assertEquals(insertDto.username(), result.getUsername());
	}

	@Test
	public void givenAlreadyUsedUsername_whenInsertNewUser_thenThrowNewException () {
		UserInsertDto insertDto = new UserInsertDto(
				"test@test.com",
				"John Doe",
				"password",
				"johndoe");

		User user1 = new User(
				"id1",
				"test@test.com",
				"John Doe",
				"password",
				"johndoe");

		when(userRepository.findByUsername(insertDto.username())).thenReturn(Optional.of(user1));

		assertThrows(UsernameAlreadyUsedException.class, () -> userService.insertNewUser(insertDto));
	}

	@Test
	public void givenAlreadyUsedEmail_whenInsertNewUser_thenThrowNewException () {
		UserInsertDto insertDto = new UserInsertDto(
				"test@test.com",
				"John Doe",
				"password",
				"johndoe");

		User user1 = new User(
				"id1",
				"test@test.com",
				"John Doe",
				"password",
				"johndoe");

		when(userRepository.findByEmail(insertDto.email())).thenReturn(Optional.of(user1));

		assertThrows(EmailAlreadyUsedException.class, () -> userService.insertNewUser(insertDto));
	}

	@Test
	public void givenValidInput_whenUpdatingUser_thenReturnDto () {
		UserInsertDto insertDto = new UserInsertDto(
				"test@test.com",
				"John Doe",
				"password",
				"johndoe");

		User user1 = new User(
				"id1",
				"test@test.com",
				"John Doe",
				"password",
				"johndoe");
		when(userRepository.findByEmail(insertDto.email())).thenReturn(Optional.of(user1));

		when(userRepository.save(user1)).thenReturn(user1);

		UserDto result = userService.updateUser("id1",insertDto);

		assertNotNull(result);
		assertEquals(insertDto.email(), result.getEmail());
		assertEquals(insertDto.name(), result.getName());
		assertEquals(insertDto.username(), result.getUsername());
	}

	@Test
	public void givenInvalidInput_whenUpdatingUser_theThrowNewException () {
		UserInsertDto insertDto = new UserInsertDto(
				"test@test.com",
				"John Doe",
				"password",
				"johndoe");


		when(userRepository.findById("NoUsedId")).thenReturn(Optional.empty());

		assertThrows(ResourceNotFoundException.class, () -> userService.updateUser("NoUsedId",insertDto));

	}


}
