package com.onboarding.crud.api.resources;

import com.onboarding.crud.api.dto.UserDto;
import com.onboarding.crud.api.dto.UserInsertDto;
import com.onboarding.crud.api.exceptions.BadRequestException;
import com.onboarding.crud.api.services.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserResource {

	private final UserService userService;

	@GetMapping
	public ResponseEntity<UserDto> findByUsernameOrEmail(
			@RequestParam(required = false) String username,
			@RequestParam(required = false) String email
	) {

		if (username != null || email != null) {
			UserDto userDto = userService.findByUsernameOrEmail(username);
			return ResponseEntity.ok().body(userDto);
		} else if (email != null) {
			UserDto userDto = userService.findByUsernameOrEmail(email);
			return ResponseEntity.ok().body(userDto);
		} else {
			throw new BadRequestException("Either 'username' or 'email' needed.");
		}

	}

	@PostMapping
	public ResponseEntity<UserDto> insertNewUser(@Valid @RequestBody UserInsertDto dto) {
		UserDto userDto = userService.insertNewUser(dto);
		return ResponseEntity.ok(userDto);
	}

	@PutMapping("/{id}")
	public ResponseEntity<UserDto> updateUser(
			@Valid @RequestBody UserInsertDto dto, @PathVariable String id
	) {
		UserDto userDto = userService.updateUser(id, dto);
		return ResponseEntity.ok(userDto);
	}

	@DeleteMapping
	public ResponseEntity<String> deleteUser (@RequestParam("username") String username, @RequestParam("email") String email) {

		if (username != null) {
			userService.deleteUser(username);
		} else if (email != null) {
			userService.deleteUser(email);
		} else {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok().body("User deleted.");
	}

}
