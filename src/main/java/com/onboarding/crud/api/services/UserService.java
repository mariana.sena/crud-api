package com.onboarding.crud.api.services;

import com.onboarding.crud.api.dto.UserDto;
import com.onboarding.crud.api.dto.UserInsertDto;
import com.onboarding.crud.api.entities.User;
import com.onboarding.crud.api.exceptions.ResourceNotFoundException;
import com.onboarding.crud.api.exceptions.UsernameAlreadyUsedException;
import com.onboarding.crud.api.producers.KafkaMessageProducer;
import com.onboarding.crud.api.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

	private final UserRepository userRepository;

	private final KafkaMessageProducer kafkaMessageProducer;

	public UserDto findByUsernameOrEmail (String field) {
		User user = userRepository.findByUsernameOrEmail(field, field)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for username/email: " + field));
		return new UserDto(user);
	}


	public UserDto insertNewUser (UserInsertDto dto) {
		User user = new User(dto);
		userRepository.findByUsernameOrEmail(dto.username(),dto.username()).ifPresent(u -> {
            throw new UsernameAlreadyUsedException("Username already in use.");
        });
		userRepository.findByUsernameOrEmail(dto.email(),dto.email()).ifPresent(u -> {
			throw new UsernameAlreadyUsedException("Email already in use.");
		});

		user = userRepository.save(user);

		kafkaMessageProducer.sendMessage(new StringBuilder().append("USER CREATED: ").append(user.getId()).toString(), "USER-ACTION" );

		return new UserDto(user);
	}



	public UserDto updateUser (String id, UserInsertDto dto) {
		User userRequested = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for id:" + id));

		userRequested.setUsername(dto.username());
		userRequested.setEmail(dto.email());
		userRequested.setName(dto.name());
		userRequested.setPassword(dto.password());

		userRequested = userRepository.save(userRequested);

		kafkaMessageProducer.sendMessage(new StringBuilder().append("USER UPDATED: ").append(userRequested.getId()).toString(), "USER-ACTION" );


		return new UserDto(userRequested);


	}

	public void deleteUser(String value){
		User userRequested = userRepository.findByUsernameOrEmail(value, value)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for Email/Username:" + value));
		userRepository.delete(userRequested);

		kafkaMessageProducer.sendMessage(new StringBuilder().append("USER DELETED: ").append(userRequested.getId()).toString(), "USER-ACTION" );

	}

}

