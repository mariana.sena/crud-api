package com.onboarding.crud.api.exceptions;

public class UsernameAlreadyUsedException extends RuntimeException {

	public UsernameAlreadyUsedException (String message) {
		super(message);
	}
}
